package monProjet;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Soldier extends Sprite{
	private int cost_production;
	private int time_production;
	private double speed;
	
	
	public Soldier(Pane layer,Image image,double x, double y,int cost_production, int time_production, double speed, int health, int damages) {
		super(layer, image, x, y, health, damages);
		this.cost_production = cost_production;
		this.time_production = time_production;
		this.speed = speed;
	}

	public int getCost_production() {
		return cost_production;
	}

	public void setCost_production(int cost_production) {
		this.cost_production = cost_production;
	}

	public int getTime_production() {
		return time_production;
	}

	public void setTime_production(int time_production) {
		this.time_production = time_production;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public void move() {
        x += dx;
        y += dy;
    }
	public boolean isAlive() {
        return health > 0;
    }

}
