package monProjet;

import java.awt.Color;
import java.util.ArrayList;
public class Player {

	private ArrayList<Chateaux> listChateaux = new ArrayList<Chateaux>();
	private Color couleur;
	
	
	public Player(ArrayList<Chateaux> listChateaux, Color couleur, Input input) {
		super();
		this.setListChateaux(listChateaux);
		this.setCouleur(couleur);
	}


	public Color getCouleur() {
		return couleur;
	}


	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}


	public ArrayList<Chateaux> getListChateaux() {
		return listChateaux;
	}


	public void setListChateaux(ArrayList<Chateaux> listChateaux) {
		this.listChateaux = listChateaux;
	}


}
