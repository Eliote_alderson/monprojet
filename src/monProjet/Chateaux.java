package monProjet;


import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Chateaux extends Sprite{
	private String duc;
	private boolean mine;
	private int treasure;
	private int level;
	private ArrayList<Onagre> troupeOnagre = new ArrayList<Onagre>();
	private ArrayList<Chevalier> troupeChevalier = new ArrayList<Chevalier>();
	private ArrayList<Piquier> troupePiquier = new ArrayList<Piquier>();
	private int unity_product;
	private String porte;
	public Chateaux(Pane layer,Image image,  double x, double y, int health, double damage,String duc, boolean mine, int treasure, int level, 
			ArrayList<Onagre> troupe_Onagre,ArrayList<Chevalier> troupe_Chevalier,ArrayList<Piquier> troupe_Piquier,int unity_product, String porte) {
		super(layer, image,x,y,health, damage);
		this.duc = duc;
		this.mine = mine;
		this.treasure = treasure;
		this.level = level;
		this.troupeOnagre = troupe_Onagre;
		this.troupeChevalier = troupe_Chevalier;
		this.troupePiquier = troupe_Piquier;
		this.unity_product = unity_product;
		this.porte = porte;
	}//ArrayList<Soldier> reserve_de_troupe,
	public String getDuc() {
		return duc;
	}
	public void setDuc(String duc) {
		this.duc = duc;
	}
	public boolean isMine() {
		return mine;
	}
	public void setMine(boolean mine) {
		this.mine = mine;
	}
	public int getTreasure() {
		return treasure;
	}
	public void setTreasure(int treasure) {
		this.treasure = treasure;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	public ArrayList<Onagre> getTroupeOnagre() {
		return troupeOnagre;
	}
	public void setTroupeOnagre(ArrayList<Onagre> troupeOnagre) {
		this.troupeOnagre = troupeOnagre;
	}
	public ArrayList<Chevalier> getTroupeChevalier() {
		return troupeChevalier;
	}
	public void setTroupeChevalier(ArrayList<Chevalier> troupeChevalier) {
		this.troupeChevalier = troupeChevalier;
	}
	public ArrayList<Piquier> getTroupePiquier() {
		return troupePiquier;
	}
	public void setTroupePiquier(ArrayList<Piquier> troupePiquier) {
		this.troupePiquier = troupePiquier;
	}
	public int getUnity_product() {
		return unity_product;
	}
	public void setUnity_product(int unity_product) {
		this.unity_product = unity_product;
	}
	public String getPorte() {
		return porte;
	}
	public void setPorte(String porte) {
		this.porte = porte;
	}
	
	
	
	
}
