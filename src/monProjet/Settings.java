package monProjet;

public class Settings {

	public static final double SCENE_WIDTH = 700;
	public static final double CHATEAUX_WIDTH = 60;
	public static final double CHATEAUX_HEIGHT = 60;
    public static final double SCENE_HEIGHT = 450;
	public static final double STATUS_BAR_HEIGHT = 50;
	
	
	
    public static final int WIDTH = 100;
    public static final int HEIGHT = 100;


    public static final double PLAYER_SPEED = 4.0;
    public static final int    PLAYER_HEALTH = 3;
    public static final double PLAYER_DAMAGE = 1;

    public static final double MISSILE_SPEED = 4.0;
    public static final int    MISSILE_HEALTH = 0;
    public static final double MISSILE_DAMAGE = 1.0;

    public static final int ENEMY_SPAWN_RANDOMNESS = 200;
    
    public static final int FIRE_FREQUENCY_LOW = 1000 * 1000 * 1000; // 1 second in nanoseconds
    public static final int FIRE_FREQUENCY_MEDIUM = 500 * 1000 * 1000; // 0.5 second in nanoseconds
    public static final int FIRE_FREQUENCY_HIGH = 100 * 1000 * 1000; // 0.1 second in nanoseconds
    
    // piquier
    
    public static final int PIQUIER_COST_PRODUCTION = 100;
    public static final int PIQUIER_TIME_PRODUCTION = 5;
    public static final int PIQUIER_SPEED = 2;
    public static final int PIQUIER_HEALTH = 1;
    public static final int PIQUIER_DAMAGES = 1;
    
    //chervalier
    
    public static final int CHEVALIER_COST_PRODUCTION = 500;
    public static final int CHEVALIER_TIME_PRODUCTION = 20;
    public static final int CHEVALIER_SPEED = 6;
    public static final int CHEVALIER_HEALTH = 3;
    public static final int CHEVALIER_DAMAGES = 5;
    
    //onagre
    
    public static final int ONAGRE_COST_PRODUCTION = 1000;
    public static final int ONAGRE_TIME_PRODUCTION = 50;
    public static final int ONAGRE_SPEED = 1;
    public static final int ONAGRE_HEALTH = 5;
    public static final int ONAGRE_DAMAGES = 10;
    
    //
    public static final int NB_PLAYER = 4;
    public static final int DISTANCE_CHATEAUX = 100;
    

}
