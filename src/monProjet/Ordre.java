package monProjet;

public class Ordre {
	private Chateaux target;
	private int nombre_troops;
	public Ordre(Chateaux target, int nombre_troops) {
		super();
		this.target = target;
		this.nombre_troops = nombre_troops;
	}
	public Chateaux getTarget() {
		return target;
	}
	public void setTarget(Chateaux target) {
		this.target = target;
	}
	public int getNombre_troops() {
		return nombre_troops;
	}
	public void setNombre_troops(int nombre_troops) {
		this.nombre_troops = nombre_troops;
	}
	
	

}
