package monProjet;


import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
public class Main extends Application{
	private Random rnd = new Random();
	private ArrayList<Onagre> troupesOnagre = new ArrayList<>();
	private ArrayList<Piquier> troupesPiquier = new ArrayList<>();
	private ArrayList<Chevalier> troupesChevalier = new ArrayList<>();
	private Pane playfieldLayer;
	private ArrayList<Chateaux> chateaux = new ArrayList<Chateaux>();
	private Image Chateaux1;
	private Image Chateaux2;
	private Image Chateaux3;
	private Image Chateaux4;
	private Image ChevalierImage;
	private Image PiquierImage;
	private Image OnagreImage;
	private Text scoreMessage = new Text();
	private Input input;

	private Group root;
	private Scene scene;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Mon Projet");
		
		root = new Group();
		scene = new Scene(root, Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT + Settings.STATUS_BAR_HEIGHT,Color.LIGHTBLUE);
		scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		playfieldLayer = new Pane();
		root.getChildren().add(playfieldLayer);
		loadGame();
    
 	
		
		
	}

	private void loadGame() {
		Chateaux1 = new Image(getClass().getResource("/images/chateauxS.png").toExternalForm(), Settings.CHATEAUX_HEIGHT, Settings.CHATEAUX_WIDTH, true, true);
		Chateaux2 = new Image(getClass().getResource("/images/chateauxE.png").toExternalForm(), Settings.CHATEAUX_HEIGHT, Settings.CHATEAUX_WIDTH, true, true);
		Chateaux3 = new Image(getClass().getResource("/images/chateauxN.png").toExternalForm(), Settings.CHATEAUX_HEIGHT, Settings.CHATEAUX_WIDTH, true, true);
		Chateaux4 = new Image(getClass().getResource("/images/chateauxO.png").toExternalForm(), Settings.CHATEAUX_HEIGHT, Settings.CHATEAUX_WIDTH, true, true);
		ChevalierImage = new Image(getClass().getResource("/images/chevalier.png").toExternalForm(), 30, 30, true, true);
		OnagreImage = new Image(getClass().getResource("/images/onagre.png").toExternalForm(), 30, 30, true, true);
		PiquierImage = new Image(getClass().getResource("/images/piquier.jpg").toExternalForm(), 30, 30, true, true);
		input = new Input(scene);
		input.addListeners();
		
		createChateaux();
		createStatusBar();
//		double x = 7, y  =8;
//		createsoldat(x,y);
	}
	
	public void createStatusBar() {
		HBox statusBar = new HBox();
		scoreMessage.setText("welcome to the kingdom of shit");
		statusBar.getChildren().addAll(scoreMessage);
		statusBar.getStyleClass().add("statusBar");
		statusBar.relocate(0, Settings.SCENE_HEIGHT);
		statusBar.setPrefSize(Settings.SCENE_WIDTH, Settings.STATUS_BAR_HEIGHT);
		root.getChildren().add(statusBar);
	}
	private void createsoldat(double x, double y) {
			x += ChevalierImage.getWidth()/2; 
			y += ChevalierImage.getHeight()/2;
			troupesChevalier.clear();
			troupesOnagre.clear();
			troupesPiquier.clear();
			for(int i = 1; i < 2; i++) {
				troupesChevalier.add(new Chevalier(playfieldLayer,ChevalierImage,x,y,Settings.CHEVALIER_COST_PRODUCTION,Settings.CHEVALIER_TIME_PRODUCTION,Settings.CHEVALIER_SPEED,Settings.CHEVALIER_HEALTH,Settings.CHEVALIER_DAMAGES));
			}
			for(int i = 1; i < 2; i++) {
				troupesOnagre.add(new Onagre(playfieldLayer,OnagreImage,x,y,Settings.ONAGRE_COST_PRODUCTION,Settings.ONAGRE_TIME_PRODUCTION,Settings.ONAGRE_SPEED,Settings.ONAGRE_HEALTH,Settings.ONAGRE_DAMAGES));
			}
			for(int i = 1; i < 2; i++) {
				troupesPiquier.add(new Piquier(playfieldLayer,PiquierImage,x,y,Settings.PIQUIER_COST_PRODUCTION,Settings.PIQUIER_TIME_PRODUCTION,Settings.PIQUIER_SPEED,Settings.PIQUIER_HEALTH,Settings.PIQUIER_DAMAGES));
			}
			
		
		
	}

	@SuppressWarnings("unused")
	private void createChateaux() {
		double vraiX,vraiY,x,y;
		boolean bool = true;
		for(int i = 0 ; i < Settings.NB_PLAYER; i++) {
			do{
				vraiX = ThreadLocalRandom.current().nextDouble(Settings.SCENE_WIDTH * 0.05,Settings.SCENE_WIDTH * 0.95 - Chateaux1.getWidth());
				vraiY = ThreadLocalRandom.current().nextDouble((Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT)* 0.05,(Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT) * 0.95 - Chateaux1.getHeight());
				if (i == 0) bool = false;
				else {
					for(int t = 0; t < chateaux.size();t++) {
						x =  chateaux.get(t).getX();
						y =  chateaux.get(t).getY();
						if(Math.abs(x - vraiX) < Settings.DISTANCE_CHATEAUX && Math.abs(y - vraiY) < Settings.DISTANCE_CHATEAUX ) break;
						else 
							if (t == chateaux.size() - 1) bool = false;
					}
				}
			}while(bool);
		
		int j = 0 ;
//		for(int i = 1 ; i <= Settings.NB_PLAYER; i++) {
//			if (i == 1) {
//				x = (Settings.SCENE_WIDTH - Chateaux1.getHeight() )* rnd.nextDouble() ;
//				y = (Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT) * 0.07;
//			}else if(i == 2){
//				x = (Settings.SCENE_WIDTH - Chateaux1.getHeight() ) * rnd.nextDouble();
//				y = (Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT) * 0.6;
//			}else if(i == 3){
//				x = (Settings.SCENE_WIDTH - Chateaux1.getHeight() ) * rnd.nextDouble();
//				y = (Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT) * 0.5;
//			}else{
//				x = (Settings.SCENE_WIDTH - Chateaux1.getHeight() ) * rnd.nextDouble();
//				y = (Settings.SCENE_HEIGHT - Settings.STATUS_BAR_HEIGHT) * 0.9;
//			}
			
			createsoldat(vraiX,vraiY);
//		vraiX += ChevalierImage.getWidth()/2; 
//		vraiY += ChevalierImage.getHeight()/2;
//		troupesChevalier.clear();
//		troupesOnagre.clear();
//		troupesPiquier.clear();
//		for(int ii = 1; ii < 2; ii++) {
//			troupesChevalier.add(new Chevalier(playfieldLayer,ChevalierImage,vraiX,vraiY,Settings.CHEVALIER_COST_PRODUCTION,Settings.CHEVALIER_TIME_PRODUCTION,Settings.CHEVALIER_SPEED,Settings.CHEVALIER_HEALTH,Settings.CHEVALIER_DAMAGES));
//		}
//		for(int ii = 1; ii < 2; ii++) {
//			troupesOnagre.add(new Onagre(playfieldLayer,OnagreImage,vraiX,vraiY,Settings.ONAGRE_COST_PRODUCTION,Settings.ONAGRE_TIME_PRODUCTION,Settings.ONAGRE_SPEED,Settings.ONAGRE_HEALTH,Settings.ONAGRE_DAMAGES));
//		}
//		for(int ii = 1; ii < 2; ii++) {
//			troupesPiquier.add(new Piquier(playfieldLayer,PiquierImage,vraiX,vraiY,Settings.PIQUIER_COST_PRODUCTION,Settings.PIQUIER_TIME_PRODUCTION,Settings.PIQUIER_SPEED,Settings.PIQUIER_HEALTH,Settings.PIQUIER_DAMAGES));
//		}
		
			j = rnd.nextInt(4);
			if(j == 1) //Integer.toString(i)
				chateaux.add(new Chateaux(playfieldLayer,Chateaux1,vraiX,vraiY,1,0,"Cherif",true,1000,1,troupesOnagre,troupesChevalier,troupesPiquier,4,"S"));
			else if(j == 2) 
				chateaux.add(new Chateaux(playfieldLayer,Chateaux2,vraiX,vraiY,1,0,"Mohamed",true,200,1,troupesOnagre,troupesChevalier,troupesPiquier,2,"E"));
			else if(j == 3) 
				chateaux.add(new Chateaux(playfieldLayer,Chateaux3,vraiX,vraiY,1,0,"Andoulaye",true,700,1,troupesOnagre,troupesChevalier,troupesPiquier,2,"N"));
			else 
				chateaux.add(new Chateaux(playfieldLayer,Chateaux4,vraiX,vraiY,1,0,"Diallo",true,5600,1,troupesOnagre,troupesChevalier,troupesPiquier,2,"O"));
		}
		
		for(Chateaux chat : chateaux) {
			chat.getView().setOnMouseClicked(e -> {
				scoreMessage.setText("Name Duc: " + chat.getDuc() +"           "+ " Level: "+ chat.getLevel() +"           "+ "Tresor: "+ chat.getTreasure());
			});
		}
		for(Chateaux chat : chateaux) {
			chat.getView().setOnMouseClicked(e -> {
//				chat.getTroupeOnagre().get(0).addToLayer();
				
			});
		}
		//chateaux.get(0).getTroupeChevalier().get(0).addToLayer();
	}
		

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
