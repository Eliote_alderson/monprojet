package monProjet;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Onagre extends Soldier{

	public Onagre(Pane layer, Image image, double x, double y,int cost_production, int time_production, int speed, int heath, int damages) {
		super(
				layer,
				image, 
				x, 
				y,
				Settings.ONAGRE_COST_PRODUCTION,
				Settings.ONAGRE_TIME_PRODUCTION,
				Settings.ONAGRE_SPEED,
				Settings.ONAGRE_HEALTH,
				Settings.ONAGRE_DAMAGES
				);
		// TODO Auto-generated constructor stub
	}

}
